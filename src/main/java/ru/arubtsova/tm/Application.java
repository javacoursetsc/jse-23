package ru.arubtsova.tm;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
