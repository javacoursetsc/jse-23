package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    AbstractCommand getCommandByArg(@Nullable String arg);

    @NotNull
    AbstractCommand getCommandByName(@Nullable String name);

    void add(@Nullable AbstractCommand command);

}
