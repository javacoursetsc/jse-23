package ru.arubtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class ArgumentsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "show application arguments.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        System.out.println("Available arguments:");
        for (@NotNull final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (!Optional.ofNullable(arg).isPresent()) continue;
            System.out.println(arg);
        }
    }

}