package ru.arubtsova.tm.enumerated;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    Role(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

}
